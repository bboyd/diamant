const fs = require('fs');
const path = require('path');
const { PrismaClient } = require('@prisma/client')
const { ApolloServer, PubSub } = require('apollo-server');
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const User = require('./resolvers/User')
const Subscription = require('./resolvers/Subscription')
const Game = require('./resolvers/Game')
const prisma = new PrismaClient()

const resolvers = {
  Query,
  Mutation,
  Subscription,
  User,
  Game,
}

const pubsub = new PubSub()
const server = new ApolloServer({
  typeDefs: fs.readFileSync(
    path.join(__dirname, 'schema.graphql'),
    'utf8'
  ),
  resolvers,
  context: ({ req }) => {
    return {
      ...req,
      prisma,
      pubsub,
    }
  }
})

server
  .listen()
  .then(({ url }) =>
    console.log(`Server is running on ${url}`)
  );
